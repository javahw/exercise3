package com.company;

import java.util.*;

public class Main {

    public String getWinner(List <String> board){
        Map<String, Integer> results = new HashMap<>();
        int max = 0;
        String winner = "";

        for (String player : board){
            String[] tokens = player.split(" ");
            if (results.containsKey(tokens[0])){
                results.put(tokens[0], results.get(tokens[0]) + Integer.parseInt(tokens[1]));
            }
            else{
                results.put(tokens[0], Integer.parseInt(tokens[1]));
            }
            if (results.get(tokens[0]) > max){
                max = results.get(tokens[0]);
                winner = tokens[0];
            }
        }
        return winner;
    }

    public static void main(String[] args) {
        Main app = new Main();
        List<String> board = new ArrayList<>();
        System.out.println("enter player results line by line and 0 at last: ");
        while (true){
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            if (input.equals("0")){
                break;
            }
            board.add(input);
        }

        String winner = app.getWinner(board);
        System.out.println("The winner is: " + winner);
    }
}
